package cardgame.cards;

import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {

    private static final char[] suit = {'S', 'H', 'D', 'C'};

    private final ArrayList<PlayingCard> playingCards;

    public DeckOfCards() {
        playingCards = new ArrayList<>();
        fillDeckWithCards();
    }

    /**
     * Fills deck with cards of all suits from 1 to 13 (Ace to King), 52 cards in total.
     */
    private void fillDeckWithCards() {
        for (int i = 0; i < 4; i++) { //4 suits
            for (int j = 1; j <= 13; j++) { //there are 13 cards in each suit
                playingCards.add(new PlayingCard(suit[i], j));
            }
        }
    }

    /**
     * Deals a given number of unique cards to a hand
     *
     * @param numberOfCards The number of cards to deal to one hand
     * @return A hand of cards
     */
    public ArrayList<PlayingCard> dealHand(int numberOfCards) throws IllegalArgumentException {
        if (numberOfCards < 1 || numberOfCards >= 52)
            throw new IllegalArgumentException("Invalid number of cards");

        ArrayList<PlayingCard> dealtCards = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < numberOfCards; i++) {
            int randomCardNumber = random.nextInt(playingCards.size());
            dealtCards.add(playingCards.get(randomCardNumber));
            playingCards.remove(randomCardNumber);
        }
        return dealtCards;
    }

    public ArrayList<PlayingCard> getPlayingCards() {
        return playingCards;
    }

    public static char[] getSuits() {
        return suit;
    }
}
