package cardgame.cards;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HandOfCards {

    private final ArrayList<PlayingCard> playingCards;

    public HandOfCards(ArrayList<PlayingCard> playingCards) {
        this.playingCards = playingCards;
    }

    public ArrayList<PlayingCard> getPlayingCards() {
        return playingCards;
    }

    //Oppgave 5:

    //1
    public String getSumOfFaces() {
        return playingCards.stream().map(PlayingCard::getFace).reduce(Integer::sum).get().toString();
    }

    //2
    public List<PlayingCard> getHeartsOnly() {
        return playingCards.stream().filter(p -> p.getSuit() == 'H').collect(Collectors.toList());
    }

    //3
    public boolean containsQueenOfSpades() {
        return playingCards.stream().anyMatch(p -> p.equals(new PlayingCard('S', 12)));
    }

    //4
    public List<PlayingCard> checkForFlush() {
        for (char suit : DeckOfCards.getSuits()) {
            if (playingCards.stream().filter(p -> p.getSuit() == suit).count() >= 5) {
                return playingCards.stream().filter(p -> p.getSuit() == suit).collect(Collectors.toList());
            }
        }
        return null;
    }
}
