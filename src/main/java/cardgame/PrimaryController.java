package cardgame;

import cardgame.cards.DeckOfCards;
import cardgame.cards.HandOfCards;
import cardgame.cards.PlayingCard;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;

import java.util.List;

public class PrimaryController {

    public FlowPane cardView;
    public Label sumOfFacesLabel;
    public Label queenOfSpadesLabel;
    public HBox heartsBox;
    public HBox flushBox;

    //called when fxml is loaded
    public void initialize() {
        dealAndCheckCards();
    }

    @FXML
    public void dealAndCheckCards() {
        DeckOfCards deck = new DeckOfCards();

        cardView.getChildren().clear();
        HandOfCards hand = new HandOfCards(deck.dealHand(10));

        for (PlayingCard playingCard : hand.getPlayingCards()) {
            VBox card = new VBox();
            card.getChildren().add(new Label(playingCard.getAsString()));
            cardView.getChildren().add(card);
        }

        checkHand(hand);
    }

    //Oppgave 5 gui del:
    private void checkHand(HandOfCards hand) {
        //1 summer kort:
        sumOfFacesLabel.setText(hand.getSumOfFaces());
        List<PlayingCard> hearts = hand.getHeartsOnly();

        //2 finn alle hjerter
        heartsBox.getChildren().clear();
        if (hearts.isEmpty()) {
            heartsBox.getChildren().add(new Label("No hearts"));
        } else {
            for (PlayingCard p : hearts) {
                Label heartLabel = new Label(p.getAsString());
                heartLabel.setTextFill(Paint.valueOf("#43A047"));
                heartsBox.getChildren().add(heartLabel);
            }
        }

        //3 sjekk etter spa dame
        if (hand.containsQueenOfSpades()) {
            queenOfSpadesLabel.setText("Yes");
            queenOfSpadesLabel.setTextFill(Paint.valueOf("#43A047"));
        } else {
            queenOfSpadesLabel.setText("No");
            queenOfSpadesLabel.setTextFill(Paint.valueOf("#000000"));
        }

        //4 sjekk etter flush
        List<PlayingCard> flush = hand.checkForFlush();
        flushBox.getChildren().clear();
        if (flush == null) {
            flushBox.getChildren().add(new Label("No flush"));
        } else {
            for (PlayingCard p : flush) {
                Label flushLabel = new Label(p.getAsString());
                flushLabel.setTextFill(Paint.valueOf("#43A047"));
                flushBox.getChildren().add(flushLabel);
            }
        }
    }
}
