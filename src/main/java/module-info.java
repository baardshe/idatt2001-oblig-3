module edu.ntnu.idatt2001 {
    requires javafx.controls;
    requires javafx.fxml;

    opens cardgame to javafx.fxml;
    exports cardgame;
    exports cardgame.cards;
}