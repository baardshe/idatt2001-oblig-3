import cardgame.cards.DeckOfCards;

import cardgame.cards.PlayingCard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class DeckOfCardsTest {

    @Test
    void correctSizeTest() {
        DeckOfCards deckOfCards = new DeckOfCards();
        assertEquals(52, deckOfCards.getPlayingCards().size());
    }

    @Test
    void getHandGivenArgument() {
        DeckOfCards deckOfCards = new DeckOfCards();
        int numberOfCards = 20;
        ArrayList<PlayingCard> hand = deckOfCards.dealHand(numberOfCards);
        assertEquals(numberOfCards, hand.size());
        assertEquals(52-numberOfCards, deckOfCards.getPlayingCards().size());
    }

    @Test
    void getHandGivenIllegalArgument() {
        DeckOfCards deckOfCards = new DeckOfCards();
        assertThrows(IllegalArgumentException.class, () -> deckOfCards.dealHand(100));
        assertThrows(IllegalArgumentException.class, () -> deckOfCards.dealHand(-1));
    }
}
